# Container Practical #
## By Sebastian, Felix and Wahid ##

## Trello ##

```
#!bin/bash
https://trello.com/invite/b/Rw6e6vob/d01f12202fbd88987dca4c14b6972490/container-practical

```

## How to spin up infrastructure ##

Run:

```
#!bin/bash
vagrant up

```

## Tasks/Roles ##

Sebastian:

1. Jenkins
2. Haproxy
3. Consul server
4. Zabbix

Felix:

1. mysql
2. wildfly
3. Docker registry

Wahid:

1. wildfly
2. onecmdb
3. consul agents
4. provisioning of VMs

### Tasks Completed ###

* Consul server can discover all services through HAProxy Template.
* MySQL has a database for onecmdb.
* Wildfly is setup for sample.war file.
* Jenkins git pulls only.
* Zabbix is just an empty monitoring container with no configurations setup.
* Docker registry for pushing images and pulling is set up for wildfly, onecmdb and haproxy.