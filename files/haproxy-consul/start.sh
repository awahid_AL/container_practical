#!/bin/bash

service haproxy start

consul-template -consul=$CONSUL_URL -template="/templates/haproxy-consul/haproxy.ctmpl:/etc/haproxy/haproxy.cfg:service haproxy reload"

