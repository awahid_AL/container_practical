#!/bin/bash

# Start the dodgy NIC
/usr/sbin/ifup enp0s8

# Ensure it restarts on reboot
echo "/usr/sbin/ifup enp0s8" >>/etc/rc.local

# Delete NetworkManager
if rpm -qa | grep NetworkManager
then
  yum erase -y NetworkManager
  systemctl start network
  systemctl enable network
fi

if ! rpm -qa | grep yum-utils >/dev/null 2>&1
then
  echo "Installing yum utils and jquery"
  yum -y install yum-utils epel-release jq
fi

if ! rpm -qa | grep yum-config-manager >/dev/null 2>&1
then
  echo "Installing yum config manager"
  yum-config-manager --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
fi

if ! rpm -qa | grep docker-ce >/dev/null 2>&1
then
  echo "Installing Docker"
  yum -y install docker-ce

fi

# Start docker
if ! service docker-ce status >/dev/null 2>&1
then
  echo "Starting Docker service"
  systemctl enable docker
  systemctl start docker
fi

# Add vagrant to docker group
if groups | grep docker
then
  usermod -G docker vagrant
fi

# Set VM with designated containers (check Trello for assignments)
sudo /vagrant/bin/setup.sh
